# Quotes-App





![screenshot](./img/screen.png)

Additional description about the project and its features.

## Built With

- HTML,
- JavaScript
- Bootstrap
- type.fit (API)

## Live Demo

[Live Demo Link](https://quotes-app-alexzambra2610-cf5ab07e790d13d2cc29cf2052ad875d3d34d.gitlab.io)




## Author

👤 **Pablo Alexis Zambrano Coral**

- Github: [@Alexoid1](https://github.com/Alexoid1)
- Twitter: [@pablo_acz](https://twitter.com/pablo_acz)
- Linkedin: [linkedin](https://www.linkedin.com/in/pablo-alexis-zambrano-coral-7a614a189/)







